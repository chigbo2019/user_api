<?php
/**
 * Created by PhpStorm.
 * User: onyinyechukwunnadi-chigbo
 * Date: 13/04/19
 * Time: 13:29
 */

namespace AppBundle\Controller\Api;


use AppBundle\Entity\User;
use AppBundle\Repository\UserEntityManager;
use AppBundle\Repository\UserRepository;
use AppBundle\Service\UserService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

class UserController extends Controller
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var UserEntityManager
     */
    private $userEntityManager;

    /**
     * @var UserService
     */
    private $userService;


    /**
     * @param UserRepository $userRepository
     * @param UserEntityManager $userEntityManager
     */
    public function __construct(UserRepository $userRepository, UserEntityManager $userEntityManager, UserService $userService)
    {
        $this->userRepository = $userRepository;
        $this->userEntityManager = $userEntityManager;
        $this->userService = $userService;
    }

    /**
     * Creates a Resource in db
     * @Route("/api/users")
     * @Method("POST")
     * @param $request
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function newAction(Request $request) {
        // Get the request body and decode it into an array
        $body = $request->getContent();
        $data = json_decode($body, true);

        // Check data exists - if it doesn't throw an HttpException
        if ($data === null) {
            throw new HttpException(400, 'Invalid Json ' . $request->getContent());
        }

        // Create user
        $user = $this->userRepository->createUser($data);

        // Validate email, firstname and lastname
        $errors = $this->userService->validate($user);
        if (!empty($errors)) {
            $data = $this->userService->handleValidationError($errors);
            return new JsonResponse($data, 400);
        }

        // save data to database
        $this->userEntityManager->save($user);

        // Get Url
        $url = $this->generateUrl('api_users_show', [
            'id' => $user->getId()
        ]);

        // Data to use for returning representation of the resource - saves user from making one more request
        $data = $this->userRepository->serializeUsers($user);

        // Encode json string, set status to 201 and set content type to json
        $response = new JsonResponse($data, 201);

        // Set header location and content type
        $response->headers->set('Location', $url);

        // Return Response
        return  $response;

    }


    /**
     * Returns a single user by id
     * @Route("/api/users/{id}", name="api_users_show")
     * @Method("GET")
     * @param $id
     * @return JsonResponse
     */
    public function showAction($id)
    {
        // Query database to get a record
        $user = $this->userRepository->findUserById($id);

        // Check if user exists - if it doesn't throw an exception
        if (!$user) {
            throw $this->createNotFoundException('No user found for id ' . $id);
        }

        // Get record details
        $data = $this->userRepository->serializeUsers($user);

        // Encode json string, set status to 200 and set content type to json
        $response = new JsonResponse($data, 200);

        // Return user as response
        return $response;

    }


    /**
     * Returns collection of users
     * @Route("/api/users")
     * @Method("GET")
     * @return JsonResponse
     */
    public function ListAction() {
        // Query database to get all records
        $users = $this->userRepository->findAllUsers();

        // Loop through user object
        $data = ['users' => []];
        foreach ($users as $user) {
            $data['user'][] = $this->userRepository->serializeUsers($user);
        }

        // Encode json string, set status to 200 and set content type to json
        $response = new JsonResponse($data, 200);

        // Return collection of users as response
        return $response;

    }


    /**
     * Updates a record
     * @Route("/api/users/{id}", name="api_users_update")
     * @Method("PUT")
     * @param $id
     * @return JsonResponse
     * @param Request $request
     * @throws \Exception
     */
    public function updateAction(Request $request, $id) {
        // Get the request body and decode it into an array
        $body = $request->getContent();
        $data = json_decode($body, true);

        // Check data exists - if it doesn't throw an exception
        if ($data === null) {
            throw new \Exception('Invalid Json ' . $request->getContent());
        }

        // Query database to get a record
        $user = $this->userRepository->findUserById($id);


        // Check if user exists - if it doesn't throw an exception
        if (!$user) {
            throw $this->createNotFoundException('No user found for id ' . $id);
        }

        // Set record fields to json data
        $setFields = $this->userRepository->setFields($user, $data);

        // Validate email, firstname and lastname
        $errors = $this->userService->validate($user);
        if (!empty($errors)) {
            $data = $this->userService->handleValidationError($errors);
            return new JsonResponse($data, 400);
        }

        // save data to database
        $this->userEntityManager->save($user);

        // Data to use for returning representation of the resource
        $data = $this->userRepository->serializeUsers($setFields);

        // Encode json string, set status to 201 and set conten type to json
        $response = new JsonResponse($data, 200);

        // Return Response
        return  $response;

    }


    /**
     * Deletes a record
     * @Route("/api/users/{id}", name="api_users_delete")
     * @Method("DELETE")
     * @param $id
     * @return Response
     */
    public function deleteAction($id) {
        // Query database to get a record
        $user = $this->userRepository->findUserById($id);

        // Delete user from db it exists
        if ($user) {
            $this->userEntityManager->delete($user);
        }

        // Return Response
        return $response = new Response(null,204);
    }

}