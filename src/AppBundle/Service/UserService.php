<?php
/**
 * Created by PhpStorm.
 * User: onyinyechukwunnadi-chigbo
 * Date: 13/04/19
 * Time: 23:01
 */

namespace AppBundle\Service;


use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserService
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @param $obj
     * @return \Symfony\Component\Validator\ConstraintViolationListInterface
     */
    public function validate($obj)
    {
        $errors = $this->validator->validate($obj);
        $errorsData = array();

        foreach ($errors as $error) {
            /** @var \Symfony\Component\Validator\ConstraintViolation $error */
            // reduces to just one error per field, that's a decision I'm making
            $errorsData[$error->getPropertyPath()] = $error->getMessage();
        }

        return $errorsData;
    }


    /**
     * @param $errors
     * @return array
     */
    public function handleValidationError($errors){
        if (!empty($errors)) {
            $data = [
                'type'=> 'validation_error',
                'title' => 'There was a validation error',
                'errors' => $errors
            ];

            return $data;
        }

    }

}