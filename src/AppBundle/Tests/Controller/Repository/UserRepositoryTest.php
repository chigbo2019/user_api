<?php
/**
 * Created by PhpStorm.
 * User: onyinyechukwunnadi-chigbo
 * Date: 13/04/19
 * Time: 16:31
 */

namespace AppBundle\Tests\Controller\Repository;


use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class BookRepositoryTest extends KernelTestCase
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
    }


    /**
     * Test createUser method
     */
    public function testWeCanCreateAUser() {

        $user = new User();
        $user->setId(1);
        $user->setFirstName('Chigbo');
        $user->setLastName('Onyinyechukwu');
        $user->setEmail('chiggy.5657@yahoo.com');
        $user->setCreatedAt('2019-12-12');

        $this->assertEquals($user->getId(), 1);
        $this->assertEquals($user->getFirstName(), 'Chigbo');
        $this->assertEquals($user->getLastName(), 'Onyinyechukwu');
        $this->assertEquals($user->getEmail(), 'chiggy.5657@yahoo.com');
        $this->assertEquals($user->getCreatedAt(), '2019-12-12');
    }

    /**
     *  Test serializeUsers
     */
    public function testWeCanSerializeUsers() {
        $data = [
            'id' => 1
        ];

        $user = new User();
        $user->setId($data['id']);

        self::assertEquals($user->getId(), $data['id']);
    }

    /**
     *  Test setFields
     */
    public function testWeCanSetUserFields() {
        $user = new User();
        $user->setFirstName('Chigbo');

        $this->assertEquals($user->getFirstName(), 'Chigbo');

    }

    public function testFindUserById()
    {
        $user = $this->entityManager
            ->getRepository(User::class)
            ->findUserById(2);

        $this->assertObjectHasAttribute('id', $user);
    }

    public function testfindAllUsers()
    {
        $users = $this->entityManager
            ->getRepository(User::class)
            ->findAllUsers() ;

        $this->assertCount(count($users), $users);
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }


}