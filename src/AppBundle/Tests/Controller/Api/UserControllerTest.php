<?php
/**
 * Created by PhpStorm.
 * User: onyinyechukwunnadi-chigbo
 * Date: 13/04/19
 * Time: 16:29
 */

namespace AppBundle\Tests\Controller\Api;


use AppBundle\Test\ApiTestCase;

class UserControllerTest extends ApiTestCase
{
    private $id = 26;

    // Test - POST request
    public function testNewAction() {
        // data to be posted
        date_default_timezone_set("Europe/London");
        $firstName = 'User';
        $email = 'chigbo'.rand(0, 999). '@yahoo.com';
        date_default_timezone_set("Europe/London");
        $data = [
            'id' => $this->id,
            'firstName' => $firstName,
            'lastName' => 'Chigbo',
            'email' => $email,
            'createdAt' => date("Y/m/d h:i:s")
        ];

        // POST Request - to create a book resource
        $request = $this->client->post('/api/users',null, json_encode($data));
        $response = $request->send();

        // Assertions
        $this->assertEquals(201, $response->getStatusCode());
        $this->assertTrue($response->hasHeader('Location'));
        $this->assertEquals('application/json', $response->getContentType());
        $this->assertEquals('/api/users/'.$data['id'] , $response->getHeader('Location'));

        $finishedData = json_decode($response->getBody(), true);
        $this->assertArrayhasKey('email', $finishedData);
    }

    // Test - GET request (one record)
    public function testShowData() {
        $userUrl = '/api/users/'.$this->id;

        // GET Request - to get a book resource
        $request = $this->client->get($userUrl); //'/api/books/'.$id
        $response = $request->send();

        // Assertions
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('application/json', $response->getContentType());
    }

    // Test - PUT request
    public function testUpdateAction() {
        // Record to modify
        $bookUrl = '/api/users/'.$this->id;
        $data = [
            'firstName' => 'New User',
            'lastName' => 'New Lastname',
            'email' => 'chiggs@yahoo.com',
            'createdAt' => date("Y/m/d h:i:s")
        ];

        // PUT Request - to modify a book resource
        $request = $this->client->put($bookUrl, null, json_encode($data));
        $response = $request->send();

        // Assertions
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('application/json', $response->getContentType());
        $finishedData = json_decode($response->getBody(), true);
        $this->assertArrayhasKey('firstName', $finishedData);

    }

    // Test - DELETE request
    public function testDeleteAction() {
        // Record to delete
        $bookUrl = '/api/users/'.$this->id;

        // DELETE Request - to delete a book resource
        $request = $this->client->delete($bookUrl);
        $response = $request->send();

        // Assertion
        $this->assertEquals(204, $response->getStatusCode());
    }


}