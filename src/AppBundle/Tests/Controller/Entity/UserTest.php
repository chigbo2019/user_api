<?php
/**
 * Created by PhpStorm.
 * User: onyinyechukwunnadi-chigbo
 * Date: 13/04/19
 * Time: 16:30
 */

namespace AppBundle\Tests\Controller\Entity;


use AppBundle\Entity\User;

class UserTest extends \PHPUnit_Framework_TestCase
{
    /**
     * Test getId()
     */
    public function testThatWeCanGetTheid() {

        $user = new User();
        $user->setId(1);

        $this->assertEquals($user->getId(), 1);

    }

    /**
     * Test getFirstName()
     */
    public function testThatWeCanGetFirstName() {

        $user = new User();
        $user->setFirstName('Chigbo');

        $this->assertEquals($user->getFirstName(), 'Chigbo');

    }

    /**
     * Test getLastName()
     */
    public function testThatWeCanGetLastName() {

        $user = new User();
        $user->setLastName('Onyinyechukwu');

        $this->assertEquals($user->getLastName(), 'Onyinyechukwu');

    }

    /**
     * Test getLastName()
     */
    public function testThatWeCanGetEmail() {

        $user = new User();
        $user->setEmail('chiggy.5657@yahoo.com');

        $this->assertEquals($user->getEmail(), 'chiggy.5657@yahoo.com');

    }

    /**
     * Test getLastName()
     */
    public function testThatWeCanGetCreationDate() {

        $user = new User();
        $user->setCreatedAt('2019-12-12');

        $this->assertEquals($user->getCreatedAt(), '2019-12-12');

    }

}