<?php
/**
 * Created by PhpStorm.
 * User: onyinyechukwunnadi-chigbo
 * Date: 13/04/19
 * Time: 14:12
 */

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class UserRepository extends EntityRepository
{
    /**
     * Create a user
     * @param $userData
     * @return User
     */
    public function createUser($userData)
    {
        $user = new User();
        return $this->setFields($user, $userData);
    }


    /**
     * Function that returns an array of Users
     * @param User $user
     * @return array
     */
    public function serializeUsers(User $user) {
        return [
            'id' => $user->getId(),
            'firstName' => $user->getFirstName(),
            'lastName' => $user->getLastName(),
            'email' => $user->getEmail(),
            'CreatedAt' => $user->getCreatedAt()
        ];
    }

    /**
     * Set User fields to data from json
     * @param User $user
     * @param $data
     * @return User
     */
    public function setFields (User $user, $data){
        $user->setFirstName($data['firstName']);
        $user->setLastName($data['lastName']);
        $user->setEmail($data['email']);
        $user->setCreatedAt(new \DateTime($data['createdAt']));

        return $user;
    }

    /**
     * Fetch a user by id
     * @param $id
     * @return object
     * $this->entityRepository
     */
    public function findUserById($id) {
        return $this->findOneBy([
            'id' => $id
        ]);
    }

    /**
     * Fetch all users
     * @return array
     * $this->entityRepository
     */
    public function findAllUsers() {
        return $this->findAll();
    }

}