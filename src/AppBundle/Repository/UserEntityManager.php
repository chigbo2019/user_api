<?php
/**
 * Created by PhpStorm.
 * User: onyinyechukwunnadi-chigbo
 * Date: 13/04/19
 * Time: 14:14
 */

namespace AppBundle\Repository;


use Doctrine\ORM\EntityManagerInterface;

class UserEntityManager
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * Save a user
     * @param $user
     */
    public function save($user) {
        $this->em->persist($user);
        $this->em->flush();
    }

    /**
     * Delete a user
     * @param $user
     */
    public function delete($user) {
        $this->em->remove($user);
        $this->em->flush();
    }

}