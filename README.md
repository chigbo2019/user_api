user_api
====================================================================

A Symfony project created on April 13, 2019, 8:27 am.

### Setup Instructions ###

1.  Run 'composer install' to install all required packages and thier dependencies

2.  Set up your server - you could use Mamp if you are on a mac or  Xammp if you are using a windows machine.

3.  Change the database config details in app/config/parameters.yml to match yours.

4.  Use doctrine command to create database and table:
    php bin/console doctrine:database:create
    php bin/console doctrine:schema:update --force
    or
    You can run the commands in user.sql (see directory sql/user.sql) to create database and user table

5.  Use following endpoints to insert, fetch, update or delete data from your database
    /api/users  - insert data. You would require to specify the data (json) to insert in postman body
    /api/users/1 - fetch a user. The value 1 can be changed as desired
    /api/users  - fetches all users
    /api/users/1  - delete a user. The value 1 can be changed as desired
    /api/users/1  - update a user. You would require to specify the data (json) to insert in postman body

    Note: You can also import postman collection json file to consume the API (see directory postman_collection/user Api.postman_collection.json)

6. To checkout test coveverage go to coverage/Test/index.html

7. To read about what was done and areas that could be improved upon if given more time checkout
     technical test docummentation.docx
