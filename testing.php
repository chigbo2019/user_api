<?php

require __DIR__.'/vendor/autoload.php';

use Guzzle\Http\Client;

// create our http client (Guzzle)
$client = new Client('http://localhost:8000', array(
    'request.options' => array(
        'exceptions' => false,
    )
));

// data to be posted
date_default_timezone_set("Europe/London");
$firstName = 'User'.rand(0, 999);
$email = 'chigbo'.rand(0, 999). '@yahoo.com';
date_default_timezone_set("Europe/London");
$data = [
    'firstName' => $firstName,
    'lastName' => 'Chigbo',
    'email' => $email,
    'createdAt' => date("Y/m/d h:i:s")
];

// POST Request - to create a user resource
$request = $client->post('/api/users',null, json_encode($data));
$response = $request->send();
$userUrl = $response->getHeader('Location');

echo $response;
echo "\n\n";

// GET Request - to get a user resource
$request = $client->get($userUrl);
$response = $request->send();

echo $response;
echo "\n\n";

// Get Request - to get all user resources
$request = $client->get('/api/users');
$response = $request->send();

echo $response;
echo "\n\n";

// Put Request - to update/modify a user resource
$data1 = [
    'firstName' => 'Updated name',
    'lastName' => 'Chigbo new',
    'email' => $email,
    'createdAt' => date("Y/m/d h:i:s")
];
$request = $client->put($userUrl, null, json_encode($data1));
$response = $request->send();

echo $response;
echo "\n\n";

// Delete Request - to delete a user resource
$request = $client->delete($userUrl);
$response = $request->send();

// display the response value
echo $response;
echo "\n\n";
die;